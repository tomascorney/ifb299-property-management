<!DOCTYPE html>
<html>
	<head>
		<title>Home Page</title>
		<link rel="stylesheet" href="css/style.css">
		<meta charset="utf-8"/>
	</head>

	<body>
		<form method="post">
			<p>
				<select name="query" onchange="this.form.submit()">
					<option selected value=""></option>
					<option value="users">Users</option>
					<option value="properties">Properties</option>
					<option value="lease_periods">Lease Periods</option>
				</select>
			</p>
		</form>
		<?php
		if (isset($_POST['query'])) {
			$con = new mysqli("localhost","root","","website");
			if (!$con) {
				printf("Couldn't connect");
			}
			if ($_POST['query'] == "users") {
				printf("<h2>Querying all users<br></h2>");
				if ($query = $con->query("SELECT * FROM users")) {
					while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
						// print_r($row);
						echo "user_id: " . $row['user_id'] . "<br>";
						echo "name: " . $row['name'] . "<br>";
						echo "email: " . $row['email'] . "<br>";
						echo "phone: " . $row['phone'] . "<br>";
						echo "password: " . $row['password'] . "<br>";
						echo "access_level: " . $row['access_level'] . "<br>";
						echo "<br>";
					}
				}
			}
			if ($_POST['query'] == "properties") {
				printf("<h2>Querying all properties<br></h2>");
				if ($query = $con->query("SELECT * FROM properties")) {
					while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
						// print_r($row);
						echo "property_id: " . $row['property_id'] . "<br>";
						echo "user_id: " . $row['user_id'] . "<br>";
						echo "address: " . $row['address'] . "<br>";
						echo "avaliable: " . $row['avaliable'] . "<br>";
						echo "property_type: " . $row['property_type'] . "<br>";
						echo "bedrooms: " . $row['bedrooms'] . "<br>";
						echo "bathrooms: " . $row['bathrooms'] . "<br>";
						echo "parking: " . $row['parking'] . "<br>";
						echo "furnished: " . $row['furnished'] . "<br>";
						echo "smoking: " . $row['smoking'] . "<br>";
						echo "pet_friendly: " . $row['pet_friendly'] . "<br>";
						echo "description: " . $row['description'] . "<br>";
						echo "<br>";
					}
				}
			}
			if ($_POST['query'] == "lease_periods") {
				printf("<h2>Querying all lease periods<br></h2>");
				if ($query = $con->query("SELECT * FROM lease_periods")) {
					while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
						// print_r($row);
						echo "property_id: " . $row['property_id'] . "<br>";
						echo "start_lease_date: " . $row['start_lease_date'] . "<br>";
						echo "lease_length: " . $row['lease_length'] . "<br>";
						echo "<br>";
					}
				}
			}
			mysqli_close($con);
		}
		?>
	</body>
</html>