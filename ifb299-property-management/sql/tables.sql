-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2015 at 04:59 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `website`
--

-- --------------------------------------------------------

--
-- Table structure for table `lease_periods`
--

CREATE TABLE IF NOT EXISTS `lease_periods` (
  `property_id` int(11) unsigned NOT NULL COMMENT 'The property''s ID',
  `start_lease_date` date NOT NULL COMMENT 'The date when the lease starts',
  `lease_length` int(11) unsigned NOT NULL COMMENT 'The amount of months this property is avaliable'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lease_periods`
--

INSERT INTO `lease_periods` (`property_id`, `start_lease_date`, `lease_length`) VALUES
(1, '2015-08-12', 8);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `property_id` int(11) unsigned NOT NULL COMMENT 'Property ID',
  `user_id` int(11) unsigned NOT NULL COMMENT 'The user''s ID of who owns this property',
  `address` varchar(80) NOT NULL DEFAULT 'This property''s manager has not given the address of this property' COMMENT 'The human readable address',
  `avaliable` tinyint(1) NOT NULL COMMENT 'Whether this house is full or not',
  `price` int(11) NOT NULL DEFAULT '-1' COMMENT 'The cost per month to live here',
  `property_type` text NOT NULL COMMENT 'What kind of property is it? Unit, Flat, Apartment, ...?',
  `bedrooms` int(11) NOT NULL COMMENT 'The amount of bedrooms in this property',
  `bathrooms` int(11) NOT NULL COMMENT 'The amount of bathrooms in this property',
  `parking` int(11) NOT NULL COMMENT 'The amount of cars that can be parked within the garage space in this property',
  `furnished` tinyint(1) NOT NULL COMMENT 'Whether the house has been furnished or not',
  `smoking` tinyint(1) NOT NULL COMMENT 'Whether smoking is allowed within this apartment',
  `pet_friendly` tinyint(1) NOT NULL COMMENT 'Whether pets are allowed in this apartment',
  `description` varchar(1000) NOT NULL DEFAULT 'This property currently has no description avaliable' COMMENT 'A description of the property'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Holds all the information about the properties';

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`property_id`, `user_id`, `address`, `avaliable`, `price`, `property_type`, `bedrooms`, `bathrooms`, `parking`, `furnished`, `smoking`, `pet_friendly`, `description`) VALUES
(1, 2, '15 Ahoy St, Caribbean, 5043', 1, 300, 'boat', 4, 4, 2, 1, 1, 1, 'This property currently has no description avaliable');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL COMMENT 'The ID of the user',
  `name` varchar(80) NOT NULL COMMENT 'The full name of the user',
  `email` varchar(80) NOT NULL COMMENT 'The email of the user',
  `phone` int(11) DEFAULT '-1' COMMENT 'The phone number of the user with no spaces',
  `password` varchar(80) NOT NULL COMMENT 'An encrypted password',
  `access_level` varchar(80) NOT NULL COMMENT 'tennant/owner/manager'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Holds all the users';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `phone`, `password`, `access_level`) VALUES
(1, 'John Smith', 'john.smith@gmail.com', 403267433, 'Bro', 'tennant'),
(2, 'Captain Jones', 'c.jones@hotmail.com', 548376625, 'ahoy', 'owner');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lease_periods`
--
ALTER TABLE `lease_periods`
  ADD PRIMARY KEY (`property_id`,`start_lease_date`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`property_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lease_periods`
--
ALTER TABLE `lease_periods`
  MODIFY `property_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The property''s ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `property_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Property ID',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The ID of the user',AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
