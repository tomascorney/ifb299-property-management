### **LOGIN** ###
Notes:
- Head to **var/www/html** to get to the files available on the website.
- The **var/www/html/dev/** folder is meant to be used for editing during the sprint process

**To edit the website:**  
Host Name/URL: [54.191.154.196](54.191.154.196)  
Port: 22  
Username: ubuntu  
  
**To edit the database:**  
54.191.154.196/phpmyadmin  
Username: root  
Password: password  

### **SETUP** ###

https://ifb-097.signin.aws.amazon.com/console  

Usernames and passwords are available on Facebook

To access/edit the website:  

1. Log into the above link with your details.
2. Click EC2.
3. Click Security Groups.
4. Click the group called "097_SecurityGroup".
5. Click the 'Inbound' tab.
6. Edit > Add Rule.  
   * Type - All Traffic  
   * Source (Dropdown box) - My IP
7. Save.  

### **OLD SETUP** ###

tables.sql located in "sql/" is a bunch of TABLES YOU NEED TO IMPORT INITIALLY.

Create a database called "website" and use "Import" to import these tables when selecting the "website" database.

Then when loading index.php via xampp make sure mysql and the apache server is running.